#include <cassert>
#include <cstdio>
#include <vector>
#include <stack>
#include <algorithm>

using picks = std::vector< std::vector<int> >;

std::vector<int> executeSMP(picks& picks, const int ppl)
{
	std::stack< int, std::vector<int> >freeMans;
	for (int i = 0; i < ppl; ++i)
	{
		freeMans.push(i);
	}
	std::vector<int>pairs(ppl, -1); // index - woman, val - man for woman, -1 means woman is free
	std::vector<int>lastProposal(ppl, 0); // helper array for man to remember index (in man list) of current woman to try

	while (!freeMans.empty())
	{
		const auto freeMan = freeMans.top();
		const int tryWoman = picks[ppl + freeMan][lastProposal[freeMan]]; // index of a woman in current man
		++lastProposal[freeMan]; // advance to next woman

		if (pairs[tryWoman] == -1)
		{
			pairs[tryWoman] = freeMan;
			freeMans.pop();
		}
		else if ( picks[tryWoman][freeMan] < picks[tryWoman][pairs[tryWoman]] )
		{
			freeMans.pop();
			freeMans.push(pairs[tryWoman]);
			pairs[tryWoman] = freeMan;
		}
	}
	return pairs; //C++11 -> no copy guaranteed
}

int main()
{
	FILE* input = nullptr;
	const errno_t errInput = fopen_s(&input, "./tests/input", "r");
	FILE* output = nullptr;
	const errno_t errOutput = fopen_s(&output, "./tests/main_out", "w");
	assert(errInput == 0 && "Failed to open file");
	assert(errOutput == 0 && "Failed to create file");

	int tests = 0, people = 0;
	fscanf_s(input, "%d%*c", &tests);
	//Main tests loop
	for (int i = 0; i < tests; ++i)
	{
		fscanf_s(input, "%d%*c", &people);
		picks picks(people * 2, std::vector<int>(people));
		//Graph loading
		for (int j = 0; j < people*2; ++j)
		{
			fscanf_s(input, "%*d%*c"); //skip unused info
			int pick = -1;
			//For womans we store rank list instead -> O(1) finding later
			if (j < people)
			{
				for (int p = 0; p < people; ++p)
				{
					fscanf_s(input, "%d%*c", &pick);
					picks[j][pick-1] = p; //-1 for 0..n indexing
				}
			}
			else
			{
				for (int p = 0; p < people; ++p)
				{
					fscanf_s(input, "%d%*c", &pick);
					picks[j][p] = pick - 1; //-1 for 0..n indexing
				}
			}
		}

		std::vector<int>solutions = executeSMP(picks, people);

		// Printing output:
		std::vector<std::pair<int, int>>formatted;
		for (auto& solution : solutions)
		{
			int i = &solution - &solutions[0];
			++i;
			formatted.emplace_back( ++solution, i );
		}
		// Formatting needed for output constraints: m(1..n) -> w instead of w(1..n) -> m
		std::sort(formatted.begin(), formatted.end());
		for (auto& element : formatted)
		{
			fprintf_s(output, "%d %d\n", element.first, element.second);
		}
	}

	fclose(input);
	fclose(output);
	return 0;
}